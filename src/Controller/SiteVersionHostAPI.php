<?php

namespace Drupal\site_version_host\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\site_version_host\Entity\Site;
use Drupal\site_version_host\SiteVersionHostHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Simple Version API.
 */
class SiteVersionHostAPI extends ControllerBase {

  /**
   * Request Stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * Get live visitors deta.
   */
  public function main() {

    $output = [];
    $output["api_version"] = 1;
    $output["time"] = time();

    $api_key = $this->requestStack->getCurrentRequest()->query->get('api_key');

    $config = SiteVersionHostHelper::getConfig(FALSE);

    if (empty($config->get('json_enabled'))) {
      $output["error"] = "Disabled";
    }
    elseif (empty($api_key)) {
      $output["error"] = "API Key is empty";
    }
    elseif ($config->get('default_api_key') !== $api_key) {
      $output["error"] = "API Key error";
    }
    else {
      try {
        $url = base64_decode($this->requestStack->getCurrentRequest()->query->get('url'));

        // Check site already exist.
        $query = $this->entityTypeManager()->getStorage('site_version_host_site')
          ->getQuery();
        $query->condition('url', $url);
        $query->accessCheck(FALSE);
        $ids = $query->execute();

        // Create entity.
        if (empty($ids)) {
          $entity = Site::create(['url' => $url]);
          $entity->save();
          $entity->updateSiteStatus();
          $output["message"] = 'Site created sccessfully';
        }
        elseif (count($ids === 1)) {
          $entity = Site::load(array_shift(array_values($ids)));
          $entity->updateSiteStatus();
          $output["message"] = 'Site update sccessfully';
        }
        else {
          $output["error"] = "Multiple sites detected with same url. Remove all from host side.";
        }

      }
      catch (\Exception $e) {
        $output["error"] = $e->getMessage();
      }

    }

    return new JsonResponse($output);
  }

}
