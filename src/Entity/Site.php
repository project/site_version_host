<?php

namespace Drupal\site_version_host\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the Site entity.
 *
 * @ContentEntityType(
 *   id = "site_version_host_site",
 *   label = @Translation("Site"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" =
 *     "Drupal\site_version_host\Entity\Controller\SitesListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\site_version_host\Entity\Form\SitesForm",
 *       "edit" = "Drupal\site_version_host\Entity\Form\SitesForm",
 *       "delete" = "Drupal\site_version_host\Form\SitesDeleteForm",
 *     },
 *     "access" = "Drupal\site_version_host\SitesAccessControlHandler",
 *   },
 *   list_cache_contexts = { "user" },
 *   base_table = "site_version_host_site",
 *   admin_permission = "site_version_host admin",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" =
 *   "/admin/structure/site_version_host/manage/sites/{site_version_host_site}",
 *     "edit-form" =
 *   "/admin/structure/site_version_host/manage/sites/{site_version_host_site}/edit",
 *     "delete-form" =
 *   "/admin/structure/site_version_host/manage/sites/{site_version_host_site}/delete",
 *     "collection" = "/admin/structure/site_version_host/manage/sites/list"
 *   },
 * )
 */
class Site extends ContentEntityBase {

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the site entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the site entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['site_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('UUID'))
      ->setDescription(t('Client site UUID'))
      ->setSettings([
        'max_length' => 128,
      ]);

    // A title.
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title. Let empty to get from client'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -50,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -50,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Site Url.
    $fields["url"] = BaseFieldDefinition::create('string')
      ->setLabel("Url")
      ->setDescription(t("Full url including arguments."))
      ->setRequired(TRUE)
      ->setSettings([
        'default_value' => '',
        'text_processing' => 0,
        'max_length' => 512,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -49,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -100,
      ]);

    // A description.
    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setDescription(t('The description.'))
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 0,
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -48,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Last update.
    $fields['lastupdate'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Last update'))
      ->setDisplayConfigurable('view', TRUE);

    // Status.
    $fields["status"] = BaseFieldDefinition::create('boolean')
      ->setLabel("Status");

    // Status.
    $fields["enabled"] = BaseFieldDefinition::create('boolean')
      ->setLabel("Enabled")
      ->setDescription(t('Enable periodic auto update.'))
      ->setDisplayOptions('form', [
        'type' => 'checkbox',
        'weight' => -45,
        'default_value' => 1,
      ]);

    // Response time.
    $fields["response_time"] = BaseFieldDefinition::create('float')
      ->setLabel("Response time");

    // HTTP code.
    $fields["http_code"] = BaseFieldDefinition::create('integer')
      ->setLabel("HTTP Code");

    // Version text.
    $fields["core"] = BaseFieldDefinition::create('string')
      ->setLabel("Core version")
      ->setDescription("For example, the framework core version")
      ->setSettings([
        'default_value' => '',
        'text_processing' => 0,
        'max_length' => 16,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ]);

    // Version text.
    $fields["version"] = BaseFieldDefinition::create('string')
      ->setLabel("Version")
      ->setSettings([
        'default_value' => '',
        'text_processing' => 0,
        'max_length' => 128,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ]);

    // build.
    $fields["build"] = BaseFieldDefinition::create('integer')
      ->setLabel("Build")
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ]);

    // Error text.
    $fields["error"] = BaseFieldDefinition::create('string')
      ->setLabel("Error")
      ->setSettings([
        'default_value' => '',
        'text_processing' => 0,
        'max_length' => 256,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ]);

    // Data.
    $fields['data'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Data (JSON)'))
      ->setSettings([
        'default_value' => '[]',
        'text_processing' => 0,
      ]);

    return $fields;
  }

  /**
   * Update the site status.
   */
  public function updateSiteStatus() {
    try {
      $this->set('status', 0);
      $this->set('lastupdate', time());
      $url = $this->get('url')->getValue()[0]['value'];

      $startTime = microtime(TRUE);
      $response = file_get_contents($url);
      $endTime = microtime(TRUE);
      $resp = json_decode($response, TRUE);

      if (isset($resp['error'])) {
        $this->set('error', $resp['error']);
      }
      else {
        $data = $resp['data'];
        $this->set('status', 1);
        $this->set('response_time', ($endTime - $startTime));
        $this->set('http_code', 200);

        $fields = self::getFieldsTypes();
        foreach ($fields['global'] as $field) {
          if (isset($data[$field])) {
            if (in_array($field, $fields['init']) && !empty($this->get($field))) {
              if (!empty($this->get($field)->getValue()[0]['value'])) {
                continue;
              }
            }
            $this->set($field, $data[$field]);
            unset($data[$field]);
          }
        }
        $this->set('data', Json::encode($data));
      }
    }
    catch (\Exception $e) {
      $this->set('error', $e->getMessage());
      $this->set('http_code', $e->getCode());
    }
    $this->save();
  }

  /**
   * Get fields list.
   */
  public static function getFieldsTypes() {
    return [
      'global' => [
        'core',
        'version',
        'build',
        'description',
        'name',
        'site_uuid',
      ],
      'init' => ['name', 'site_uuid'],
    ];
  }

  /**
   * Update site status provides by id.
   */
  public static function updateSiteStatusFor($id) {
    \Drupal::logger("site_version_host")->info("Processing ID : $id");
    $entity = Site::load($id);
    $entity->updateSiteStatus();
  }

  /**
   * End of the batch process.
   */
  public static function updateSiteStatusEnd() {
    \Drupal::logger("site_version_host")->info("Processing end");
  }

}
