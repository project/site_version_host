<?php

namespace Drupal\site_version_host\Entity\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Url;
use Drupal\site_version_host\SiteVersionHostHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for Site entity.
 *
 * @ingroup site_version_host
 */
class SitesListBuilder extends EntityListBuilder {

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;


  /**
   * Module configuration.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('url_generator')
    );
  }

  /**
   * Constructs a new SitesListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, UrlGeneratorInterface $url_generator) {
    parent::__construct($entity_type, $storage);
    $this->urlGenerator = $url_generator;
  }

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    $build['description'] = [
      '#markup' => $this->t('Sites list. <a href="@adminlink">Sites admin page</a>.', [
        '@adminlink' => Url::fromRoute('site_version_host.settings')->toString(),
      ]),
    ];
    $build['table'] = parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * Building the header and content lines for the sites list.
   *
   * Calling the parent::buildHeader() adds a column for the possible actions
   * and inserts the 'edit' and 'delete' links as defined for the entity type.
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['title'] = $this->t('Name');
    $header['version'] = $this->t('Version');
    $header['build'] = $this->t('Build');
    $header['core'] = $this->t('Core');
    $header['status'] = $this->t('Status');

    // Extra fields.
    $this->config = SiteVersionHostHelper::getConfig();
    if (!empty($this->config->get('extra_fields'))) {
      $header['extra'] = $this->t('Extra');
    }
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\site_version_host\Entity\Site $entity */
    $row['id'] = $entity->toLink($entity->id());
    $row['title'] = $entity->toLink();

    $row['version'] = $entity->get('version')->getValue()[0]['value'];
    $row['build'] = $entity->get('build')->getValue()[0]['value'];
    $row['core'] = $entity->get('core')->getValue()[0]['value'];
    $row['status'] = ($entity->get('status')
      ->getValue()[0]['value']) ? "OK" : "Error";
    if (!$entity->get('enabled')->getValue()[0]['value']) {
      $row['status'] .= " (Disabled)";
    }

    // Extra fields.
    if (!empty($extra_fields = $this->config->get('extra_fields'))) {
      $row['extra'] = '';
      $data = Json::decode($entity->get('data')->getValue()[0]['value']);
      if (!empty($data)) {
        $extra_fields = explode("\n", $extra_fields);
        $values = [];
        foreach ($extra_fields as $field) {
          $field = trim($field);
          if (isset($data[$field])) {
            $values[$field] = $data[$field];
          }
        }
        $row['extra'] = implode(', ', $values);
      }
    };

    return $row + parent::buildRow($entity);
  }

}
