<?php

namespace Drupal\site_version_host\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Language\Language;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Form controller for the site_version_host_site entity edit forms.
 *
 * @ingroup site_version_host
 */
class SitesForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\site_version_host\Entity\Site $entity */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    $form['langcode'] = [
      '#title' => $this->t('Language'),
      '#type' => 'language_select',
      '#default_value' => $entity->getUntranslated()->language()->getId(),
      '#languages' => Language::STATE_ALL,
    ];

    if ($this->entity->isNew()) {
      $link = Url::fromRoute('site_version_host.settings')->toString();
      $form['info_autofonfig'] = [
        '#markup' => "<div><b>Note :</b> For the Site Version info module, Use auto configuration <a href='$link'>link</a>.</div>",
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.site_version_host_site.collection');
    $entity = $this->getEntity();
    $entity->save();
  }

}
