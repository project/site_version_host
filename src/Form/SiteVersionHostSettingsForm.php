<?php

namespace Drupal\site_version_host\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\site_version_host\SiteVersionHostHelper;

/**
 * Class SiteVersionHost Settings Form.
 */
class SiteVersionHostSettingsForm extends ConfigFormBase {

  /**
   * Get From ID.
   */
  public function getFormId() {
    return 'site_version_host_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = SiteVersionHostHelper::getConfig(FALSE);
    $host = $this->getRequest()->getSchemeAndHttpHost();
    $json_api_key = $config->get('default_api_key');

    // API.
    $form['json'] = [
      '#type' => 'details',
      '#title' => $this->t("API"),
      '#open' => $config->get('json_enabled') ? TRUE : FALSE,
    ];
    $form['json']['json_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Enable site version auto config API"),
      '#default_value' => $config->get('json_enabled'),
    ];
    $form['json']['default_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Access API KEY"),
      '#default_value' => $json_api_key,
    ];
    $link = "$host/site-version/autoconfig?api_key=$json_api_key";
    $form['json']['json_api_key_display'] = [
      '#type' => 'markup',
      '#markup' => "<p><a href='$link'>$link</a></p>",
    ];

    // Fields to display.
    $form['extra'] = [
      '#type' => 'details',
      '#title' => $this->t("Extra"),
      '#open' => empty($config->get('extra_fields')) ? FALSE : TRUE,
    ];
    $form['extra']['extra_fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Extra fields to display"),
      '#description' => $this->t("Extra data fields, One by line."),
      '#default_value' => $config->get('extra_fields'),
      '#default_value' => $config->get('extra_fields'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('default_api_key'))) {
      $form_state->setErrorByName('default_api_key', $this->t('API Key must not empty.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get config.
    $config = SiteVersionHostHelper::getConfig(TRUE);

    // Update.
    $fields = [
      'default_api_key',
      'json_enabled',
      'extra_fields',
    ];
    foreach ($fields as $field) {
      $config->set($field, $form_state->getValue($field));
    }

    // Save config.
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      SiteVersionHostHelper::getConfigName(),
    ];
  }

}
