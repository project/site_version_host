<?php

namespace Drupal\site_version_host\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\site_version_host\SiteVersionHostHelper;

/**
 * Class CheckSites Form.
 */
class CheckSitesForm extends ConfirmFormBase {

  /**
   * Get From ID.
   */
  public function getFormId() {
    return 'site_version_host_check_all';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form[] = [
      '#markup' => 'Update all enabled sites status may take few minuts depend on number of auto update enabled sites.',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $query = \Drupal::entityQuery('site_version_host_site');
    $query->condition('enabled', '1');
    $query->accessCheck(FALSE);
    $ids = $query->execute();

    if (empty($ids)) {
      $form_state->setErrorByName('submit', $this->t('No items to process.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $query = \Drupal::entityQuery('site_version_host_site');
    $query->condition('enabled', '1');
    $query->accessCheck(FALSE);
    $ids = $query->execute();

    $batch = [
      'title' => $this->t('Sites status checking ...'),
      'operations' => [],
      'init_message' => $this->t('Commencing'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('An error occurred during processing'),
      'finished' => '\Drupal\site_version_host\Entity\Site::updateSiteStatusEnd',
    ];
    foreach ($ids as $value) {
      $batch['operations'][] = [
        '\Drupal\site_version_host\Entity\Site::updateSiteStatusFor',
        [$value],
      ];
    }

    batch_set($batch);

  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      SiteVersionHostHelper::getConfigName(),
    ];
  }

  /**
   * Returns the question to ask the user.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to update all sites now ?');
  }

  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return \Drupal\Core\Url
   *   A URL object.
   */
  public function getCancelUrl() {
    return new Url('entity.site_version_host_site.collection');
  }

}
