<?php

namespace Drupal\site_version_host;

/**
 * Site Version Helper functions.
 */
class SiteVersionHostHelper {

  /**
   * Get Configuration Name.
   */
  public static function getConfigName() {
    return 'site_version_host.settings';
  }

  /**
   * Get Configuration Object.
   *
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   *   The configuration object.
   */
  public static function getConfig($editable = FALSE) {
    if ($editable) {
      $config = \Drupal::configFactory()->getEditable(static::getConfigName());
    }
    else {
      $config = \Drupal::config(static::getConfigName());
    }
    return $config;
  }

  /**
   * Generate a random key.
   */
  public static function generateKey() {
    $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    return substr(str_shuffle($str_result), 0, 32);
  }

}
